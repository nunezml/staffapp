# StaffApp Service

## Department Service

#### Rest Layer

| Action            | Method  | URL |
|-------------------|---------|-----|
| Get List by department id  | GET | /staffapp-service/department/find?id=# |
| Create New | POST | /staffapp-service/department/create |
| Get departments simple view | GET | /staffapp-service/department/list?summary=true |

Request body example:
```
{
    description: "Department Name",
    employees: [ 
        // employees list 
    ]
}
```

Response DTO:
```
{
    id: 1
    description: "Department Name",
    employees: [
        //employees list
    ]
}
```

#### DAO Layer

Department Entity

| Field         | Type    |
|---------------|---------|
| id            | long          |
| description   | String        |
| employees     | Set<Employee> |

## Employee Service

#### Rest Layer

| Action            | Method  | URL |
|-------------------|---------|-----|
| Get List by employee id  | GET | /staffapp-service/employee/find?id=# |
| Create New | POST | /staffapp-service/employee/create |
| Get employee's profile pic | GET | /staffapp-service/employee/getPic?id=# |

Request body example:
```
{
    firstName: "FirstName",
    middleName: "MiddleName",
    lastName: "LastName",
    salary: 
    jobTitle: {
        id: 1
    },
    department: {
        id: 1
    }
}
```

Response DTO:
```
{
    id: 1
    firstName: "FirstName",
    middleName: "MiddleName",
    lastName: "LastName",
    salary: 400,
    department: {
        id: 1
    },
    jobtitle: {
        id: 1,
        description: "Job Title",
        minSalary: 123,
        maxSalary: 999
    }
}
```

#### DAO Layer

Employee Entity

| Field         | Type    |
|---------------|---------|
| id            | long          |
| firstName     | String        |
| middleName    | String        |
| lastName      | String        |
| salary        | Long          |
| profilePic    | Byte[]        |
| department    | Department    |
| jobTitle      | JobTitle      |

## JobTitle Service

#### Rest Layer

| Action            | Method  | URL |
|-------------------|---------|-----|
| Get List by jobTitle id  | GET | /staffapp-service/jobtitle/find?id=# |
| Create New/Update | POST | /staffapp-service/jobtitle/save-update |

Request body example:
```
{
    description: "Job Title",
    maxSalary: 999,
    minSalary: 123
}
```

Response DTO:
```
{
    id: 1
    description: "Job Title",
    maxSalary: 999,
    minSalary: 123
}
```

#### DAO Layer

JobTitle Entity

| Field         | Type    |
|---------------|---------|
| id            | long      |
| description   | String    |
| maxSalary     | long      |
| minSalary     | long      |
