package com.staffapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "job_titles")
public class JobTitle extends BaseEntity {

    @Column(name = "name")
    private String name;
    @Column(name = "min_salary")
    private Long minSalary;
    @Column(name = "max_salary")
    private Long maxSalary;

    public String getName() {
        return name;
    }

    public void setName(String jobTitle) {
        this.name = jobTitle;
    }

    public Long getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(Long minSalary) {
        this.minSalary = minSalary;
    }

    public Long getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(Long maxSalary) {
        this.maxSalary = maxSalary;
    }
}
