package com.staffapp.client;

import com.staffapp.model.Department;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.HttpMethod.GET;

public class StaffAppClient {

    private final RestTemplate restTemplate;

    public StaffAppClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Department getDepartmentById(Long departmentId) {
        HttpHeaders requestHeaders = new HttpHeaders();
        HttpEntity<Department> entity = new HttpEntity<>(null, requestHeaders);
        return restTemplate.exchange("/department/find?id={departmentId}",
                GET, entity, Department.class, departmentId).getBody();
    }

}
