package com.staffapp.service.impl;

import com.staffapp.StaffappServerApp;
import com.staffapp.model.JobTitle;
import com.staffapp.repositories.JobTitleRepository;
import com.staffapp.service.JobTitleService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles("integration-test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StaffappServerApp.class)
public class JobTitleServiceImplTest {

    @Mock
    JobTitleRepository repository;

    JobTitleService service;

    @Rule
    public MockitoRule initRule = MockitoJUnit.rule();

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    JobTitle jobTitle1;
    JobTitle jobTitle2;
    Set<JobTitle> jobTitles;

    @Before
    public void setUp() {
        service = new JobTitleServiceImpl(repository);

        jobTitles = new HashSet<>();

        jobTitle1 = new JobTitle();
        jobTitle1.setName("Job Title 1");
        jobTitle1.setId(1L);

        jobTitle2 = new JobTitle();
        jobTitle2.setName("Job Title 2");
        jobTitle2.setId(2L);

        jobTitles.add(jobTitle1);
        jobTitles.add(jobTitle2);
    }

    @Test
    public void findAll() {
        when(repository.findAll()).thenReturn(jobTitles);

        Set<JobTitle> returnedJobTitles = service.findAll();
        assertThat(returnedJobTitles, Matchers.notNullValue());
        assertThat(returnedJobTitles, Matchers.hasSize(2));
        verify(repository, times(1)).findAll();
    }

    @Test
    public void findById() {
        when(repository.findById(anyLong())).thenReturn(java.util.Optional.ofNullable(jobTitle1));

        JobTitle returnedJobTitle = service.findById(1L);
        assertThat(returnedJobTitle, Matchers.notNullValue());
        verify(repository, times(1)).findById(anyLong());
    }

    @Test
    public void findByDescriptionOnly() {
        when(repository.findByIdOrName(any(), anyString())).thenReturn(jobTitle1);

        JobTitle foundJobTitle = service.findByIdOrName(null, "Job Title 1");
        assertNotNull(foundJobTitle);
        assertEquals("Job Title 1", foundJobTitle.getName());
        verify(repository).findByIdOrName(any(), anyString());
    }

    @Test
    public void findByIdOnly() {
        when(repository.findByIdOrName(anyLong(), any())).thenReturn(jobTitle1);

        JobTitle foundJobTitle = service.findByIdOrName(1L, null);
        assertNotNull(foundJobTitle);
        assertEquals("Job Title 1", foundJobTitle.getName());
        verify(repository).findByIdOrName(anyLong(), any());
    }

    @Test
    public void findById_NotFound() {
        when(repository.findById(anyLong())).thenReturn(null);

        exceptionRule.expect(RuntimeException.class);
        JobTitle foundJobTitle = service.findById(1L);
        assertNull(foundJobTitle);
    }

    @Test
    public void save_NotNull_Exists() {
        when(repository.save(any())).thenReturn(jobTitle1);
        when(repository.existsById(anyLong())).thenReturn(true);

        assertThat(service.save(jobTitle1), Matchers.notNullValue());
        verify(repository, times(1)).save(any());
    }

    @Test
    public void save_NotNull_NotExists() {
        when(repository.existsById(anyLong())).thenReturn(false);

        assertThat(service.save(jobTitle1), Matchers.nullValue());
        verify(repository, times(0)).save(any());
    }

    @Test
    public void save_Null() {
        assertThat(service.save(null), Matchers.nullValue());

        verify(repository, times(0)).save(any());
    }

    @Test
    public void create_Null() {
        assertThat(service.create(null), Matchers.nullValue());
        verify(repository, times(0)).save(any());
    }

    @Test
    public void create_NotNull_Exists() {
        when(repository.existsById(anyLong())).thenReturn(true);

        assertThat(service.create(jobTitle1), Matchers.nullValue());
        verify(repository, times(0)).save(any());
    }

    @Test
    public void create_NotNull_NotExists() {
        when(repository.existsById(anyLong())).thenReturn(false);

        assertThat(service.create(jobTitle1), Matchers.nullValue());
        verify(repository, times(1)).save(any());
    }

    @Test
    public void delete() {
        service.delete(jobTitle1);
        verify(repository).delete(any());
    }

    @Test
    public void deleteById() {
        service.deleteById(1L);
        verify(repository).deleteById(anyLong());
    }

    @Test
    public void deleteAll() {
        when(repository.findAll()).thenReturn(jobTitles);

        service.deleteAll();
        verify(repository).deleteAll();
    }
}