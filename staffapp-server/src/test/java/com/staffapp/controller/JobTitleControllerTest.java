package com.staffapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.staffapp.StaffappServerApp;
import com.staffapp.model.JobTitle;
import com.staffapp.service.JobTitleService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles("integration-test")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = StaffappServerApp.class)
public class JobTitleControllerTest {
    private static final String LIST_ALL = "/jobtitle/list";
    private static final String FIND_ID = "/jobtitle/find?id=1";
    private static final String FIND_INVALID = "/jobtitle/find?desc=Description";
    private static final String FIND_NAME = "/jobtitle/find?name=Job%20Title1";
    private static final String UPDATE = "/jobtitle/update";
    private static final String CREATE = "/jobtitle/create";
    private static final String DELETE_ID = "/jobtitle/delete?id=1";
    private static final String DELETE_ALL = "/jobtitle/deleteAll";

    @Mock
    JobTitleService jobTitleService;

    @InjectMocks
    JobTitleController controller;

    Set<JobTitle> jobTitles;
    JobTitle jobTitle1;
    JobTitle jobTitle2;

    MockMvc mockMvc;

    @Before
    public void setUp() {
        jobTitles = new HashSet<>();
        jobTitle1 = new JobTitle();
        jobTitle1.setName("Job Title1");
        jobTitle1.setId(1L);
        jobTitle1.setMaxSalary(999L);
        jobTitle1.setMinSalary(0L);
        jobTitle2 = new JobTitle();
        jobTitle2.setName("Job Title2");
        jobTitle1.setId(2L);
        jobTitle2.setMaxSalary(999L);
        jobTitle2.setMinSalary(0L);
        jobTitles.add(jobTitle1);
        jobTitles.add(jobTitle2);

        mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
    }

    @Test
    public void getAllJobTitles() throws Exception {
        when(jobTitleService.findAll()).thenReturn(jobTitles);

        mockMvc.perform(get(LIST_ALL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(2)));
    }

    @Test
    public void findByIdOnly() throws Exception {
        when(jobTitleService.findByIdOrName(anyLong(), any())).thenReturn(jobTitle1);

        mockMvc.perform(get(FIND_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(4)))
                .andExpect(jsonPath("$.name").value("Job Title1"));
    }

    @Test
    public void findByDescriptionOnly() throws Exception {
        when(jobTitleService.findByIdOrName(any(), anyString())).thenReturn(jobTitle1);

        mockMvc.perform(get(FIND_NAME))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(4)))
                .andExpect(jsonPath("$.name").value("Job Title1"));
    }

    @Test
    public void find_InvalidParameters() throws Exception {
        when(jobTitleService.findByIdOrName(anyLong(), any())).thenReturn(jobTitle1);

        mockMvc.perform(get(FIND_INVALID))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("Parameter id or name required"));
    }

    @Test
    public void find_NotFound() throws Exception {
        when(jobTitleService.findByIdOrName(any(), any())).thenReturn(null);

        mockMvc.perform(get(FIND_ID))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("Job Title with requested parameters not found"));
    }

    @Test
    public void createJobTitle() throws Exception {
        when(jobTitleService.create(any())).thenReturn(jobTitle1);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(jobTitle1);

        mockMvc.perform(post(CREATE).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("Job Title1"))
                .andDo(print());
    }

    @Test
    public void createJobTitle_Failed() throws Exception {
        when(jobTitleService.create(any())).thenReturn(null);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(jobTitle1);

        mockMvc.perform(post(CREATE).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isInternalServerError())
                .andExpect(status().reason("Failed to create job title entity"));
    }

    @Test
    public void updateJobTitle() throws Exception {
        when(jobTitleService.save(any())).thenReturn(jobTitle1);
        when(jobTitleService.findById(anyLong())).thenReturn(jobTitle1);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(jobTitle1);

        mockMvc.perform(put(UPDATE).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("Job Title1"));
    }

    @Test
    public void updateJobTitle_NotFound() throws Exception {
        when(jobTitleService.findById(anyLong())).thenReturn(null);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(jobTitle1);

        mockMvc.perform(put(UPDATE).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("Requested job title not found"));
        verify(jobTitleService, times(0)).save(any());
    }

    @Test
    public void updateJobTitle_Failed() throws Exception {
        when(jobTitleService.findById(anyLong())).thenReturn(jobTitle1);
        when(jobTitleService.save(any(JobTitle.class))).thenReturn(null);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(jobTitle1);

        mockMvc.perform(put(UPDATE).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isInternalServerError())
                .andExpect(status().reason("Failed to update job title entity"));
    }

    @Test
    public void deleteById() throws Exception {
        when(jobTitleService.findById(anyLong())).thenReturn(jobTitle1);

        mockMvc.perform(delete(DELETE_ID))
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void deleteById_NotFound() throws Exception {
        mockMvc.perform(delete(DELETE_ID))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("Job Title with requested Id not found"));
    }

    @Test
    public void deleteAllJobTitles() throws Exception {
        mockMvc.perform(delete(DELETE_ALL))
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("$").doesNotExist());
    }
}