package com.staffapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.staffapp.StaffappServerApp;
import com.staffapp.model.Department;
import com.staffapp.service.DepartmentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles("integration-test")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = StaffappServerApp.class)
public class DepartmentControllerTest {
    private final static String LIST_ALL = "/department/list";
    private final static String LIST_ALL_SIMPLE_VIEW = "/department/list?summary=true";
    private final static String FIND_ID = "/department/find?id=1";
    private final static String FIND_NAME = "/department/find?name=Department";
    private final static String FIND_DESCRIPTION = "/department/find?desc=Department";
    private final static String NEW = "/department/create";
    private final static String UPDATE = "/department/update";
    private final static String DELETE_ID = "/department/delete?id=1";
    private final static String DELETE_ALL = "/department/deleteAll";

    @Mock
    DepartmentService departmentService;

    @InjectMocks
    DepartmentController controller;

    Set<Department> departments;
    Department department1;
    Department department2;

    MockMvc mockMvc;

    @Before
    public void setUp() {
        departments = new HashSet<>();

        department1 = new Department();
        department1.setId(1L);
        department1.setName("Department");

        department2 = new Department();
        department2.setId(2L);
        department2.setName("Department");

        departments.add(department1);
        departments.add(department2);

        mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
    }

    @Test
    public void getAllDepartments() throws Exception {
        when(departmentService.findAll()).thenReturn(departments);

        mockMvc.perform(get(LIST_ALL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andExpect(jsonPath("$[0]*", hasKey("employees")));
    }

    @Test
    public void getAllDepartments_SummaryView() throws Exception {
        when(departmentService.findAll()).thenReturn(departments);

        mockMvc.perform(get(LIST_ALL_SIMPLE_VIEW))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andExpect(jsonPath("$[0].employees*").doesNotExist());
    }

    @Test
    public void findByIdOnly() throws Exception {
        when(departmentService.findByIdOrName(anyLong(), any())).thenReturn(department1);

        mockMvc.perform(get(FIND_ID).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(3)))
                .andExpect(jsonPath("$.name").value("Department"));
    }

    @Test
    public void findByDescriptionOnly() throws Exception {
        when(departmentService.findByIdOrName(any(), anyString())).thenReturn(department1);

        mockMvc.perform(get(FIND_NAME))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(3)))
                .andExpect(jsonPath("$.name").value("Department"));
    }

    @Test
    public void findDepartment_NotFound() throws Exception {
        when(departmentService.findByIdOrName(any(), any())).thenReturn(null);

        mockMvc.perform(get(FIND_NAME))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("Department with requested Id or name not found"));

        mockMvc.perform(get(FIND_ID))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("Department with requested Id or name not found"));
    }

    @Test
    public void findDepartment_InvalidParameters() throws Exception {
        when(departmentService.findByIdOrName(any(), any())).thenReturn(null);

        mockMvc.perform(get(FIND_DESCRIPTION))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("Parameter id or name missing"));
    }

    @Test
    public void createNewDepartment() throws Exception {
        when(departmentService.create(any(Department.class))).thenReturn(department1);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        mapper.setFilterProvider( new SimpleFilterProvider()
                .addFilter("department_filter", SimpleBeanPropertyFilter.serializeAll()));
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(department1);

        mockMvc.perform(post(NEW).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("Department"));
    }

    @Test
    public void createNewDepartment_Failed() throws Exception {
        when(departmentService.create(any(Department.class))).thenReturn(null);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        mapper.setFilterProvider( new SimpleFilterProvider()
                .addFilter("department_filter", SimpleBeanPropertyFilter.serializeAll()));
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(department1);

        mockMvc.perform(post(NEW).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isInternalServerError())
                .andExpect(status().reason("Failed to create department entity"));
    }

    @Test
    public void updateDepartment() throws Exception {
        when(departmentService.findById(anyLong())).thenReturn(department1);
        when(departmentService.save(any(Department.class))).thenReturn(department1);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        mapper.setFilterProvider( new SimpleFilterProvider()
                .addFilter("department_filter", SimpleBeanPropertyFilter.serializeAll()));
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(department1);

        mockMvc.perform(put(UPDATE).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value("1"));
    }

    @Test
    public void updateDepartment_NotFound() throws Exception {
        when(departmentService.findById(anyLong())).thenReturn(null);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        mapper.setFilterProvider( new SimpleFilterProvider()
                .addFilter("department_filter", SimpleBeanPropertyFilter.serializeAll()));
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(department1);

        mockMvc.perform(put(UPDATE).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isInternalServerError())
                .andExpect(status().reason("Failed to update department entity"));
        verify(departmentService, times(0)).save(any());
    }

    @Test
    public void updateDepartment_Failed() throws Exception {
        when(departmentService.findById(anyLong())).thenReturn(department1);
        when(departmentService.save(any(Department.class))).thenReturn(null);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        mapper.setFilterProvider( new SimpleFilterProvider()
                .addFilter("department_filter", SimpleBeanPropertyFilter.serializeAll()));
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(department1);

        mockMvc.perform(put(UPDATE).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isInternalServerError())
                .andExpect(status().reason("Failed to update department entity"));
    }

    @Test
    public void deleteById() throws Exception {
        when(departmentService.findById(anyLong())).thenReturn(department1);

        mockMvc.perform(delete(DELETE_ID))
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void deleteById_NotFound() throws Exception {
        when(departmentService.findById(anyLong())).thenReturn(null);

        mockMvc.perform(delete(DELETE_ID))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("Department with requested Id not found"));
    }

    @Test
    public void deleteAllDepartments() throws Exception {
        mockMvc.perform(delete(DELETE_ALL))
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("$").doesNotExist());
    }
}