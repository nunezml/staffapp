package com.staffapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.staffapp.StaffappServerApp;
import com.staffapp.model.Department;
import com.staffapp.model.Employee;
import com.staffapp.model.JobTitle;
import com.staffapp.service.EmployeeService;
import com.staffapp.util.ImageManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles("integration-test")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = StaffappServerApp.class)
public class EmployeeControllerTest {

    private final static String LIST_ALL = "/employee/list";
    private final static String FIND_FIRST_NAME = "/employee/find?firstName=FirstName";
    private final static String FIND_LAST_NAME = "/employee/find?lastName=LastName";
    private final static String FIND_LAST_AND_FIRST_NAME = "/employee/find?firstName=FirstName&lastName=LastName";
    private final static String FIND_LAST_AND_FIRST_NAME_AND_MIDDLE_NAME =
            "/employee/find?firstName=FirstName&lastName=LastName&middleName=MiddleName";
    private final static String GET_BY_ID = "/employee/get?id=1";
    private final static String ADD_NEW = "/employee/create";
    private final static String UPDATE = "/employee/update";
    private final static String DELETE_BY_ID = "/employee/delete?id=1";
    private final static String DELETE_ALL = "/employee/deleteAll";
    private final static String DOWNLOAD_PIC = "/employee/downloadPic?id=1";
    private final static String UPLOAD_PIC = "/employee/uploadPic?id=1";
    private final static String FIND_BAD_REQUEST = "/employee/find?name=FirstName";

    @Mock
    EmployeeService employeeService;

    @InjectMocks
    EmployeeController controller;

    Set<Employee> employees;
    Employee employee1;
    Employee employee2;
    Department department;
    JobTitle jobTitle;

    MockMvc mockMvc;

    @Before
    public void setUp() {
        employees = new HashSet<>();

        department = new Department();
        department.setId(1L);
        department.setName("Department");

        jobTitle = new JobTitle();
        jobTitle.setId(1L);
        jobTitle.setName("JobTitle");

        employee1 = new Employee();
        employee1.setId(1L);
        employee1.setFirstName("FirstName");
        employee1.setMiddleName("MiddleName");
        employee1.setLastName("LastName");
        employee1.setDepartment(department);
        employee1.setJobTitle(jobTitle);

        employee2 = new Employee();
        employee2.setId(2L);
        employee2.setFirstName("FirstName");
        employee2.setMiddleName("MiddleName");
        employee2.setLastName("LastName");
        employee2.setDepartment(department);
        employee2.setJobTitle(jobTitle);

        employees.add(employee1);
        employees.add(employee2);

        mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .build();
    }

    @Test
    public void getAllEmployees() throws Exception {
        when(employeeService.findAll()).thenReturn(employees);

        mockMvc.perform(get(LIST_ALL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(2)));
    }

    @Test
    public void getByNames_NoMiddleName() throws Exception {
        when(employeeService.findByNames(anyString(), anyString(), any())).thenReturn(employees);

        mockMvc.perform(get(FIND_LAST_AND_FIRST_NAME))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(2)));
    }

    @Test
    public void getByNames_FirstName() throws Exception {
        when(employeeService.findByNames(anyString(), any(), any())).thenReturn(employees);

        mockMvc.perform(get(FIND_FIRST_NAME))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(2)));
    }

    @Test
    public void getByNames_LastName() throws Exception {
        when(employeeService.findByNames(any(), anyString(), any())).thenReturn(employees);

        mockMvc.perform(get(FIND_LAST_NAME))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(2)));
    }

    @Test
    public void getByNames_All() throws Exception {
        when(employeeService.findByNames(anyString(), anyString(), anyString())).thenReturn(employees);

        mockMvc.perform(get(FIND_LAST_AND_FIRST_NAME_AND_MIDDLE_NAME))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(2)));
    }

    @Test
    public void getByNames_InvalidParameters() throws Exception {

        mockMvc.perform(get(FIND_BAD_REQUEST))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("Parameter firstName, lastName or middleName required"));
    }

    @Test
    public void getByNames_NotFound() throws Exception {
        when(employeeService.findByNames(anyString(), anyString(), anyString())).thenReturn(new HashSet<>());

        mockMvc.perform(get(FIND_LAST_AND_FIRST_NAME_AND_MIDDLE_NAME))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("Employee with requested parameters not found"));
    }

    @Test
    public void getById() throws Exception {
        when(employeeService.findById(anyLong())).thenReturn(employee1);

        mockMvc.perform(get(GET_BY_ID).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(7)))
                .andExpect(jsonPath("$.firstName").value("FirstName"))
                .andExpect(jsonPath("$.lastName").value("LastName"))
                .andExpect(jsonPath("$.middleName").value("MiddleName"))
                .andExpect(jsonPath("$.department.name").value("Department"))
                .andExpect(jsonPath("$.jobTitle.name").value("JobTitle"));
    }

    @Test
    public void getById_NotFound() throws Exception {
        when(employeeService.findById(anyLong())).thenReturn(null);

        mockMvc.perform(get(GET_BY_ID))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("Employee with requested Id not found"));
    }

    @Test
    public void addNewEmployee() throws Exception {
        Employee employee = new Employee();
        employee.setFirstName("Employee");
        employee.setLastName("LastName");
        employee.setDepartment(department);
        employee.setJobTitle(jobTitle);

        when(employeeService.create(any(Employee.class))).thenReturn(employee1);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        mapper.setFilterProvider( new SimpleFilterProvider()
                .addFilter("department_filter", SimpleBeanPropertyFilter.serializeAll()));
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(employee);

        mockMvc.perform(post(ADD_NEW).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.firstName").value("FirstName"));
    }

    @Test
    public void addNewEmployee_Failed() throws Exception {
        Employee employee = new Employee();
        employee.setFirstName("Employee");
        employee.setLastName("LastName");
        employee.setDepartment(department);
        employee.setJobTitle(jobTitle);

        when(employeeService.create(any(Employee.class))).thenReturn(null);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        mapper.setFilterProvider( new SimpleFilterProvider()
                .addFilter("department_filter", SimpleBeanPropertyFilter.serializeAll()));
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(employee);

        mockMvc.perform(post(ADD_NEW).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isInternalServerError())
                .andExpect(status().reason("Failed to create employee entity"));
    }

    @Test
    public void updateEmployee() throws Exception {
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setFirstName("Employee");
        employee.setLastName("LastName");
        employee.setDepartment(department);
        employee.setJobTitle(jobTitle);

        when(employeeService.findById(anyLong())).thenReturn(employee);
        when(employeeService.save(any(Employee.class))).thenReturn(employee);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        mapper.setFilterProvider( new SimpleFilterProvider()
                .addFilter("department_filter", SimpleBeanPropertyFilter.serializeAll()));
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(employee);

        mockMvc.perform(put(UPDATE).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value("1"));
    }

    @Test
    public void updateEmployee_NotFound() throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        mapper.setFilterProvider( new SimpleFilterProvider()
                .addFilter("department_filter", SimpleBeanPropertyFilter.serializeAll()));
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(employee1);

        when(employeeService.findById(anyLong())).thenReturn(null);

        mockMvc.perform(put(UPDATE).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isInternalServerError())
                .andExpect(status().reason("Failed to update employee entity"));
        verify(employeeService, times(0)).save(any());
    }

    @Test
    public void updateEmployee_Failed() throws Exception {
        when(employeeService.findById(anyLong())).thenReturn(employee1);
        when(employeeService.save(any(Employee.class))).thenReturn(null);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        mapper.setFilterProvider( new SimpleFilterProvider()
                .addFilter("department_filter", SimpleBeanPropertyFilter.serializeAll()));
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(employee1);

        mockMvc.perform(put(UPDATE).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isInternalServerError())
                .andExpect(status().reason("Failed to update employee entity"));
    }

    @Test
    public void uploadProfilePic() throws Exception {
        Employee employee = new Employee();
        employee.setDepartment(department);
        employee.setJobTitle(jobTitle);
        employee.setProfilePic(
                ImageManager.convertImgFileToByteArray("images/default-profile-pic.png"));

        MockMultipartFile profilePic = new MockMultipartFile("profilePic",
                ImageManager.convertByteArrayTobyteArray(
                        ImageManager.convertImgFileToByteArray("images/default-profile-pic.png")));

        when(employeeService.findById(anyLong())).thenReturn(employee);
        when(employeeService.save(any(Employee.class))).thenReturn(employee);

        mockMvc.perform(MockMvcRequestBuilders.multipart(UPLOAD_PIC).file(profilePic))
                .andExpect(status().isNoContent());
    }

    @Test
    public void uploadProfilePic_EmployeeNotFound() throws Exception {
        Employee employee = new Employee();
        employee.setDepartment(department);
        employee.setJobTitle(jobTitle);
        employee.setProfilePic(
                ImageManager.convertImgFileToByteArray("images/default-profile-pic.png"));

        MockMultipartFile profilePic = new MockMultipartFile("profilePic",
                ImageManager.convertByteArrayTobyteArray(
                        ImageManager.convertImgFileToByteArray("images/default-profile-pic.png")));

        when(employeeService.findById(anyLong())).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.multipart(UPLOAD_PIC).file(profilePic))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("Employee with requested Id not found"));
        verify(employeeService, times(0)).save(any());
    }

    @Test
    public void getProfilePic() throws Exception {
        Employee employee = new Employee();
        employee.setDepartment(department);
        employee.setJobTitle(jobTitle);
        employee.setProfilePic(
                ImageManager.convertImgFileToByteArray("images/default-profile-pic.png"));

        when(employeeService.findById(anyLong())).thenReturn(employee);

        mockMvc.perform(get(DOWNLOAD_PIC))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()));
    }

    @Test
    public void getProfilePic_EmployeeNotFound() throws Exception {
        Employee employee = new Employee();
        employee.setDepartment(department);
        employee.setJobTitle(jobTitle);
        employee.setProfilePic(
                ImageManager.convertImgFileToByteArray("images/default-profile-pic.png"));

        when(employeeService.findById(anyLong())).thenReturn(null);

        mockMvc.perform(get(DOWNLOAD_PIC))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("Employee with requested Id not found"));
    }

    @Test
    public void deleteById() throws Exception {
        when(employeeService.findById(anyLong())).thenReturn(employee1);

        mockMvc.perform(delete(DELETE_BY_ID))
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void deleteById_NotFound() throws Exception {
        when(employeeService.findById(anyLong())).thenReturn(null);

        mockMvc.perform(delete(DELETE_BY_ID))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("Employee with requested Id not found"));
    }

    @Test
    public void deleteAllEmployees() throws Exception {
        mockMvc.perform(delete(DELETE_ALL))
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("$").doesNotExist());
    }
}