package com.staffapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StaffappServerApp {

	public static void main(String[] args) {
		SpringApplication.run(StaffappServerApp.class, args);
	}

}
