package com.staffapp.service;

import com.staffapp.model.JobTitle;

public interface JobTitleService extends CrudService<JobTitle, Long> {
    JobTitle findByIdOrName(Long id, String name);
    JobTitle create(JobTitle newJobTitle);
}
