package com.staffapp.service.impl;

import com.staffapp.model.Department;
import com.staffapp.model.Employee;
import com.staffapp.repositories.DepartmentRepository;
import com.staffapp.repositories.EmployeeRepository;
import com.staffapp.service.DepartmentService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    private final DepartmentRepository departmentRepository;
    private final EmployeeRepository employeeRepository;

    public DepartmentServiceImpl(DepartmentRepository departmentRepository, EmployeeRepository employeeRepository) {
        this.departmentRepository = departmentRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Set<Department> findAll() {
        Set<Department> departments = new HashSet<>();
        departmentRepository.findAll().forEach(departments::add);
        return departments;
    }

    @Override
    public Department findById(Long aLong) {
        return departmentRepository.findById(aLong).orElse(null);
    }

    @Override
    public Department save(Department department) {
        if (department != null) {
            if (department.getEmployees() != null) {
                department.getEmployees().forEach(employee -> {
                    if (employee.getJobTitle() != null) {
                        if (employee.getJobTitle().getId() == null) {
                            throw new IllegalArgumentException("Employee's Job Title must be an existent Job Title");
                        }
                    } else {
                        throw new IllegalArgumentException("Employee's Job Title is required");
                    }

                    if (employee.getId() == null) {
                        employee.setId(employeeRepository.save(employee).getId());
                    }
                });
            }

            if(department.getId() != null) {
                Department existingDepartment = departmentRepository.findById(department.getId()).orElseThrow();
                Set<Employee> existingEmployees = existingDepartment.getEmployees();
                existingEmployees.addAll(department.getEmployees());
                existingDepartment.setEmployees(existingEmployees);
                if(!existingDepartment.getName().equals(department.getName())) {
                    existingDepartment.setName(department.getName());
                }
                return departmentRepository.save(existingDepartment);
            }
        }
        return null;
    }

    @Override
    public void delete(Department object) {
        departmentRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        if (departmentRepository.existsById(aLong)) {
            Department departmentToDelete = assignNullDepToAllEmployees(departmentRepository.findById(aLong).orElseThrow());
            departmentRepository.deleteById(departmentToDelete.getId());
        }
    }

    @Override
    public void deleteAll() {

        for(Department department : departmentRepository.findAll()) {
            assignNullDepToAllEmployees(department);
        }
        departmentRepository.deleteAll();

    }

    @Override
    public Department create(Department newDepartment) {
        if(newDepartment != null &&
                (newDepartment.getId() == null || !departmentRepository.existsById(newDepartment.getId()))){
                    return departmentRepository.save(newDepartment);
        }
        return null;
    }

    @Override
    public Department findByIdOrName(Long id, String name) {
        return departmentRepository.findByIdOrName(id, name);
    }

    private Department assignNullDepToAllEmployees(Department department) {
        if(!department.getEmployees().isEmpty()) {
            for (Employee employee : department.getEmployees()) {
                employee.setDepartment(null);
            }
            employeeRepository.saveAll(department.getEmployees());
        }
        return department;
    }
}
