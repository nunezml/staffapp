package com.staffapp.service;

import com.staffapp.model.Employee;

import java.util.Set;

public interface EmployeeService extends CrudService<Employee, Long> {
    Employee create(Employee newEmployee);
    Set<Employee> findByNames(String firstName, String lastName, String middleName);
}
