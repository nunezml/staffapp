package com.staffapp.service;


import com.staffapp.model.Department;

public interface DepartmentService extends CrudService<Department, Long> {
    Department create(Department newDepartment);
    Department findByIdOrName(Long id, String name);
}
