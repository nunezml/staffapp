package com.staffapp.util;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.http.converter.json.MappingJacksonValue;

import java.util.Set;

public class JsonMapping {

    private JsonMapping() {}

    public static MappingJacksonValue summarizePOJO(Object objectToSerialize, String filterName,
                                                    boolean summarize, Set<String> exceptions) {
        SimpleBeanPropertyFilter simpleBeanPropertyFilter;
        if(summarize) {
            simpleBeanPropertyFilter = SimpleBeanPropertyFilter.serializeAllExcept(exceptions);
        } else {
            simpleBeanPropertyFilter = SimpleBeanPropertyFilter.serializeAll();
        }
        FilterProvider filterProvider = new SimpleFilterProvider()
                .addFilter(filterName, simpleBeanPropertyFilter);
        MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(objectToSerialize);
        mappingJacksonValue.setFilters(filterProvider);

        return mappingJacksonValue;
    }
}
