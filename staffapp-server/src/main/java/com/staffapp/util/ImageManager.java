package com.staffapp.util;

import org.springframework.core.io.ClassPathResource;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ImageManager {

    private ImageManager() {}

    public static Byte[] convertMultipartFileToByteArray(MultipartFile file) throws IOException{
        Byte[] byteObjects = new Byte[file.getBytes().length];
        int i = 0;
        for(byte b : file.getBytes()) {
            byteObjects[i++] = b;
        }
        return byteObjects;
    }

    public static Byte[] convertImgFileToByteArray(String imgPath) throws IOException {
        File img = new ClassPathResource(imgPath).getFile();
        byte[] imagebytes = Files.readAllBytes(img.toPath());
        Byte[] imageBytes = new Byte[imagebytes.length];
        int i = 0;
        for (byte b : imagebytes) {
            imageBytes[i++] = b;
        }
        return imageBytes;
    }

    public static void convertByteArrayToPNG(Byte[] bImage, String path) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(convertByteArrayTobyteArray(bImage));
        BufferedImage bImage2 = ImageIO.read(bis);
        ImageIO.write(bImage2, "png", new File(path) );
    }

    public static byte[] convertByteArrayTobyteArray(Byte[] data) {
        byte[] bytes = new byte[data.length];
        int i = 0;
        for (Byte B : data) {
            bytes[i++] = B;
        }
        return bytes;
    }
}
