package com.staffapp.controller;

import com.staffapp.model.Department;
import com.staffapp.service.DepartmentService;
import com.staffapp.util.JsonMapping;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "/department", produces = {MediaType.APPLICATION_JSON_VALUE})
public class DepartmentController {

    private final DepartmentService departmentService;

    private static final String jsonFilterName = "department_filter";
    private final Set<String> filterExceptions = new HashSet<>(Collections.singletonList("employees"));

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping("/list")
    public Object getAllDepartments(
            @RequestParam(name = "summary", required = false, defaultValue = "false") boolean summary) {
        return JsonMapping.summarizePOJO(departmentService.findAll(), jsonFilterName,  summary, filterExceptions);
    }

    @GetMapping(path = "/find")
    public Object findByIdOrDescription(@RequestParam(name = "id", required = false) Long id,
                                        @RequestParam(name = "name", required = false) String name,
                                        @RequestParam(name = "summary", required = false,
                                                defaultValue = "false") boolean summary) {

        if(id == null && name == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Parameter id or name missing");
        }
        Department foundDep = departmentService.findByIdOrName(id, name);
        if (foundDep != null) {
            return JsonMapping.summarizePOJO(foundDep, jsonFilterName, summary, filterExceptions);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Department with requested Id or name not found");
        }
    }

    @PostMapping("/create")
    public Object addNewDepartment(@RequestBody Department newDepartment) {
        Department createdDep = departmentService.create(newDepartment);
        if(createdDep != null) {
            return JsonMapping.summarizePOJO(createdDep,
                    jsonFilterName, false, filterExceptions);
        } else {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to create department entity");
        }
    }

    @PutMapping("/update")
    public Object updateDepartment(@RequestBody Department department) {
        if (departmentService.findById(department.getId()) != null) {
            Department savedDep = departmentService.save(department);
            if(savedDep != null) {
                return JsonMapping.summarizePOJO(savedDep, jsonFilterName, false, filterExceptions);
            }
        }
        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to update department entity");
    }

    @DeleteMapping("/delete")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@RequestParam("id") Long id, HttpServletResponse servletResponse) {
        if (departmentService.findById(id) != null) {
            departmentService.deleteById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Department with requested Id not found");
        }
    }

    @DeleteMapping("/deleteAll")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAllDepartments() {
        departmentService.deleteAll();
    }

}
