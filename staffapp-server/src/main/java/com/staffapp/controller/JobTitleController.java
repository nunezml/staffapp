package com.staffapp.controller;

import com.staffapp.model.JobTitle;
import com.staffapp.service.JobTitleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;

@RequestMapping(path = "jobtitle", produces = {MediaType.APPLICATION_JSON_VALUE})
@RestController
public class JobTitleController {

    private final JobTitleService jobTitleService;

    public JobTitleController(JobTitleService jobTitleService) {
        this.jobTitleService = jobTitleService;
    }

    @GetMapping("/list")
    public Set<JobTitle> getAllJobTitles() {
        return jobTitleService.findAll();
    }

    @GetMapping("/find")
    public JobTitle findByIdOrName(@RequestParam(name = "id", required = false) Long id,
                                   @RequestParam(name = "name", required = false) String name) {

        if(id == null && name == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Parameter id or name required");
        }
        JobTitle foundJobtitle = jobTitleService.findByIdOrName(id, name);
        if(foundJobtitle != null) {
            return foundJobtitle;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Job Title with requested parameters not found");
        }
    }

    @PostMapping("/create")
    public JobTitle createJobTitle(@RequestBody JobTitle newJobTitle) {
        JobTitle createdJobTitle = jobTitleService.create(newJobTitle);
        if(createdJobTitle != null) {
            return createdJobTitle;
        }
        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to create job title entity");
    }

    @PutMapping("/update")
    public JobTitle updateJobTitle(@RequestBody JobTitle jobTitle) {
        if(jobTitleService.findById(jobTitle.getId()) != null) {
            JobTitle updatedJobTitle = jobTitleService.save(jobTitle);
            if(updatedJobTitle != null) {
                return updatedJobTitle;
            }
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to update job title entity");
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Requested job title not found");

    }

    @DeleteMapping("/delete")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@RequestParam Long id) {
        if (jobTitleService.findById(id) != null) {
            jobTitleService.deleteById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Job Title with requested Id not found");
        }
    }

    @DeleteMapping("/deleteAll")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAllJobTitles() {
        jobTitleService.deleteAll();
    }
}
