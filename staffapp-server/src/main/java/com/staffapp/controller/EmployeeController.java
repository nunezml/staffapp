package com.staffapp.controller;

import com.staffapp.model.Employee;
import com.staffapp.service.EmployeeService;
import com.staffapp.util.ImageManager;
import com.staffapp.util.JsonMapping;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@RequestMapping("/employee")
@RestController
public class EmployeeController {
    private final EmployeeService employeeService;

    private static final String jsonFilterName = "department_filter";
    private final Set<String> filterExceptions = new HashSet<>(Collections.singletonList("employees"));

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/list")
    public Object getAllEmployees() {
        return JsonMapping.summarizePOJO(employeeService.findAll(),
                jsonFilterName, true, filterExceptions);
    }

    @GetMapping("/find")
    public Object getByNames(
            @RequestParam(name = "firstName", required = false) String firstName,
            @RequestParam(name = "lastName", required = false) String lastName,
            @RequestParam(name = "middleName", required = false) String middleName) {

        if(firstName == null && lastName == null && middleName == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Parameter firstName, lastName or middleName required");
        }

        Set<Employee> foundEmployees = employeeService.findByNames(firstName, lastName, middleName);
        if(!foundEmployees.isEmpty()) {
            return JsonMapping.summarizePOJO(foundEmployees, jsonFilterName, true, filterExceptions);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with requested parameters not found");
        }
    }

    @GetMapping("/get")
    public Object getById(@RequestParam Long id) {
        Employee foundEmployee = employeeService.findById(id);
        if(foundEmployee != null) {
            return JsonMapping.summarizePOJO(foundEmployee, jsonFilterName, true, filterExceptions);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with requested Id not found");
        }
    }

    @PostMapping("/create")
    public Object addNewEmployee(@RequestBody Employee newEmployee) {
        if(newEmployee.getProfilePic() == null) {
            try {
                newEmployee.setProfilePic(
                        ImageManager.convertImgFileToByteArray("static/images/default-profile-pic.png"));
            } catch (IOException e) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                        "Failed to create employee entity");
            }
        }
        Employee createdEmployee = employeeService.create(newEmployee);
        if(createdEmployee != null) {
            return JsonMapping.summarizePOJO(createdEmployee, jsonFilterName, true, filterExceptions);
        } else {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to create employee entity");
        }
    }

    @PutMapping("/update")
    public Object updateEmployee(@RequestBody Employee employee) {
        if (employeeService.findById(employee.getId()) != null) {
            Employee savedEmployee = employeeService.save(employee);
            if(savedEmployee != null) {
                return JsonMapping.summarizePOJO(savedEmployee, jsonFilterName, true, filterExceptions);
            }
        }
        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to update employee entity");
    }

    @PostMapping("/uploadPic")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void uploadProfilePic(@RequestParam Long id, @RequestPart MultipartFile profilePic) {
        Employee employee = employeeService.findById(id);
        if(employee != null) {
            try {
                employee.setProfilePic(ImageManager.convertMultipartFileToByteArray(profilePic));
            } catch (IOException e) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to upload image");
            }
            employeeService.save(employee);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with requested Id not found");
        }
    }

    @GetMapping(value = "/downloadPic", produces = MediaType.IMAGE_PNG_VALUE)
    @ResponseBody
    public byte[] getProfilePic(@RequestParam Long id) {
        Employee employee = employeeService.findById(id);
        if(employee != null) {
            try {
                return ImageManager.convertByteArrayTobyteArray(employee.getProfilePic());
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                        "Failed to download profile picture");
            }
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with requested Id not found");
    }

    @DeleteMapping("/delete")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@RequestParam Long id) {
        if (employeeService.findById(id) != null) {
            employeeService.deleteById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with requested Id not found");
        }
    }

    @DeleteMapping("/deleteAll")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAllEmployees() {
        employeeService.deleteAll();
    }

}

