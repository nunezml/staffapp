package com.staffapp.bootstrap;

import com.staffapp.model.Department;
import com.staffapp.model.Employee;
import com.staffapp.model.JobTitle;
import com.staffapp.service.DepartmentService;
import com.staffapp.service.EmployeeService;
import com.staffapp.service.JobTitleService;
import com.staffapp.util.ImageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class DataLoader implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(DataLoader.class);

    private static final String defaultProfPicPath = "static/images/default-profile-pic.png";
    private final DepartmentService departmentService;
    private final EmployeeService employeeService;
    private final JobTitleService jobTitleService;

    @Autowired
    private DataSource dataSource;

    public DataLoader(DepartmentService departmentService, EmployeeService employeeService, JobTitleService jobTitleService) {
        this.departmentService = departmentService;
        this.employeeService = employeeService;
        this.jobTitleService = jobTitleService;
    }

    @Override
    public void run(String... args)  {

        if (departmentService.findAll().isEmpty() && employeeService.findAll().isEmpty() &&
                jobTitleService.findAll().isEmpty()) {
            try(Connection connection = dataSource.getConnection();
                Statement stmt = connection.createStatement()) {

                stmt.addBatch("SET FOREIGN_KEY_CHECKS = 0");
                stmt.addBatch("TRUNCATE TABLE employees_departments");
                stmt.addBatch("TRUNCATE TABLE employees_job_titles");
                stmt.addBatch("TRUNCATE TABLE employees");
                stmt.addBatch("TRUNCATE TABLE departments");
                stmt.addBatch("TRUNCATE TABLE job_titles");
                stmt.addBatch("SET FOREIGN_KEY_CHECKS = 1");
                stmt.executeBatch();
                log.info("DB has no entities, truncating db tables");

                initializeDBData();
            } catch (SQLException e){
                log.error("Couldn't load data to db", e);
            }
        }
    }

    private void initializeDBData()  {
        //Create Job Titles
        JobTitle engineer = new JobTitle();
        engineer.setName("Engineer");
        engineer.setMinSalary(123L);
        engineer.setMaxSalary(999L);
        jobTitleService.create(engineer);

        JobTitle manager = new JobTitle();
        manager.setName("Manager");
        manager.setMinSalary(123L);
        manager.setMaxSalary(999L);
        jobTitleService.create(manager);

        JobTitle salesRep = new JobTitle();
        salesRep.setName("Sales Representative");
        salesRep.setMinSalary(123L);
        salesRep.setMaxSalary(999L);
        jobTitleService.create(salesRep);

        JobTitle adminAss = new JobTitle();
        adminAss.setName("Administrative Assistant");
        adminAss.setMinSalary(123L);
        adminAss.setMaxSalary(999L);
        jobTitleService.create(adminAss);

        log.info("Job Titles loaded to db");

        //Create departments
        Department hr = new Department();
        hr.setName("Human Resources");
        Department rAndD = new Department();
        rAndD.setName("Research And Development");
        Department sales = new Department();
        sales.setName("Sales");

        departmentService.create(hr);
        departmentService.create(rAndD);
        departmentService.create(sales);

        log.info("Departments loaded to db");

        //Create employees
        Employee mark = new Employee();
        mark.setFirstName("Mark");
        mark.setMiddleName("Anthony");
        mark.setLastName("Muniz");
        mark.setJobTitle(manager);
        mark.setDepartment(hr);
        mark.setSalary(500L);
        try {
            mark.setProfilePic(ImageManager.convertImgFileToByteArray(defaultProfPicPath));
        } catch (IOException e) {
            log.error("Failed adding profile picture to employee", e);
        }
        employeeService.create(mark);

        Employee jane = new Employee();
        jane.setFirstName("Jane");
        jane.setLastName("Thomas");
        jane.setJobTitle(engineer);
        jane.setDepartment(rAndD);
        jane.setSalary(400L);
        try {
            jane.setProfilePic(ImageManager.convertImgFileToByteArray(defaultProfPicPath));
        } catch (IOException e) {
            log.error("Failed adding profile picture to employee", e);
        }
        employeeService.create(jane);

        Employee john = new Employee();
        john.setFirstName("John");
        john.setLastName("Thomas");
        john.setJobTitle(engineer);
        john.setDepartment(rAndD);
        john.setSalary(400L);
        try {
            john.setProfilePic(ImageManager.convertImgFileToByteArray(defaultProfPicPath));
        } catch (IOException e) {
            log.error("Failed adding profile picture to employee", e);
        }
        employeeService.create(john);

        Employee mary = new Employee();
        mary.setFirstName("Mary");
        mary.setLastName("Sue");
        mary.setJobTitle(salesRep);
        mary.setDepartment(sales);
        mary.setSalary(200L);
        try {
            mary.setProfilePic(ImageManager.convertImgFileToByteArray(defaultProfPicPath));
        } catch (IOException e) {
            log.error("Failed adding profile picture to employee", e);
        }
        employeeService.create(mary);

        log.info("Employees loaded to db");

    }
}
