CREATE DATABASE IF NOT EXISTS staffappdb;

ALTER DATABASE staffappdb
    DEFAULT CHARACTER SET utf8
    DEFAULT COLLATE utf8_general_ci;

GRANT ALL PRIVILEGES ON staffappdb.* TO 'staffappuser'@'%';